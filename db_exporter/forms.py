from typing import Tuple
from bootstrap_datepicker_plus import DatePickerInput

from django import forms

GEEKS_CHOICES = (
    ("1", "One"),
    ("2", "Two"),
    ("3", "Three"),
    ("4", "Four"),
    ("5", "Five"),
)


class RequestDatesExportForm(forms.Form):
    date_from = forms.DateField(widget=DatePickerInput())
    date_to = forms.DateField(widget=DatePickerInput())


class RequestSuitesExportForm(forms.Form):
    def __init__(self, suite_name_choices: Tuple[Tuple[str, str]], *args, **kwargs):
        self.base_fields['suite_name'].choices = suite_name_choices
        super().__init__(*args, **kwargs)

    suite_name = forms.ChoiceField(choices=())
