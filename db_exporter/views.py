from datetime import datetime
from typing import Optional

from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, redirect
from .forms import RequestDatesExportForm, RequestSuitesExportForm
from .services import (
    get_export_login,
    get_export_database,
    get_export_password,
    get_export_server, get_suite_names_by_date, get_tests_by_suite_names_and_dates
)
from .tables import TestCasesResultsTable


def select_slot_view(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        request_export_form = RequestDatesExportForm(data=request.POST)
        if request_export_form.is_valid():
            request.session['date_from'] = str(request_export_form.cleaned_data["date_from"])
            request.session["date_to"] = str(request_export_form.cleaned_data["date_to"])

            return redirect("select_suite_name")
        else:
            print("INVALID!")
    else:
        request_export_form = RequestDatesExportForm()

    context = {
        "form": request_export_form,
        "login": get_export_login(),
        "server": get_export_server(),
        "password": get_export_password(),
        "database": get_export_database(),
    }

    return render(request, "db_exporter/form.html", context=context)


def select_suite_name_view(request: HttpRequest) -> HttpResponse:
    date_from_str = request.session.get("date_from", None)
    date_to_str = request.session.get("date_to", None)

    if not (date_from_str and date_to_str):
        return redirect("export")

    date_from = datetime.strptime(date_from_str, "%Y-%m-%d")
    date_to = datetime.strptime(date_to_str, "%Y-%m-%d")

    choices = get_suite_names_by_date(date_from, date_to)

    if request.method == "POST":
        select_suite_name_form = RequestSuitesExportForm(choices, data=request.POST)

        if select_suite_name_form.is_valid():
            request.session['suite_name'] = select_suite_name_form.cleaned_data["suite_name"]
            return redirect("test_cases_result_table")
    else:
        select_suite_name_form = RequestSuitesExportForm(choices)

    context = {
        "form": select_suite_name_form,
        "login": get_export_login(),
        "server": get_export_server(),
        "password": get_export_password(),
        "database": get_export_database(),
    }

    return render(request, "db_exporter/form.html", context=context)


def test_cases_result_table(request: HttpRequest) -> HttpResponse:
    date_from_str = request.session.get("date_from", None)
    date_to_str = request.session.get("date_to", None)
    suite_name = request.session.get("suite_name", None)

    if not (date_from_str and date_to_str):
        return redirect("export")

    if not suite_name:
        return redirect("select_suite_name")

    date_from = datetime.strptime(date_from_str, "%Y-%m-%d")
    date_to = datetime.strptime(date_to_str, "%Y-%m-%d")

    test_cases = get_tests_by_suite_names_and_dates(suite_name, date_from, date_to)
    table = TestCasesResultsTable(test_cases)
    table.order_by = "version_under_test"
    return render(request, "db_exporter/table.html", context={"table": table})
